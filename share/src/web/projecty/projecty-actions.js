(function() {

	ProjectY = {

		actions : {

			onActionDeclareAsClaim : function onActionDeclareAsClaim(node) {
				// $this will be the instance of component we merged into,
				// either Alfresco.DocumentList or Alfresco.DocumentActions
				var $this = this;

				var jsNode = node.jsNode;
				var nodeRef = jsNode.nodeRef.nodeRef;

				// Intercept before dialog show
				var doBeforeDialogShow = function dlA_onActionDetails_doBeforeDialogShow(p_form, p_dialog) {

					var fileSpan = '<span class="light">' + Alfresco.util.encodeHTML(node.displayName) + '</span>';
					
					Alfresco.util.populateHTML(
							[p_dialog.id + "-form-container_h", $this.msg("declare-as-claim" + ".title", fileSpan) 
							 
				    ]);
				};

				var loadingMsgDialog = Alfresco.util.PopupManager.displayMessage({
					displayTime : 0, // infinite
					modal : true, // user cannot click outside
					text : "<span class='wait'>" + $this.msg("label.loading") + "</span>",
					noEscape : true
				});

				// Read more on "Javascript Constants" section below
				var formDisplayUrl = Alfresco.constants.URL_SERVICECONTEXT
						+ "components/form?itemKind={itemKind}&itemId={itemId}&destination={destination}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true";

				// Substitute templates with real values
				formDisplayUrl = YAHOO.lang.substitute(formDisplayUrl, {
					itemKind : "node",
					itemId : nodeRef,
					mode : "edit",
					submitType : "json",
					formId : "declare-as-claim"
				});

				// this is our Alfresco webscript
				var formSubmitUrl = Alfresco.constants.PROXY_URI + "com/yellresearch/claim/declare";
				var username = Alfresco.constants.USERNAME;

				// Use Alfresco provided component: SimpleDialog
				// an alfresco instance will always have an ID
				var dialog = new Alfresco.module.SimpleDialog($this.id + "-declare-as-claim");

				dialog.setOptions({
					width : "40em",
					templateUrl : formDisplayUrl,
					actionUrl : formSubmitUrl,
					destroyOnHide : true,
					doBeforeDialogShow : {
						fn : doBeforeDialogShow,
						scope : this
					},
					doBeforeFormSubmit : {
						fn : function doBeforeFormSubmit(form) {
									
							var hiddenInput = document.createElement("input");
							hiddenInput.type = "hidden";
							hiddenInput.name = "nodeRef";
							hiddenInput.value = nodeRef;
							var formEl = YAHOO.util.Dom.get(form.id);
							formEl.appendChild(hiddenInput);

							var hidden = document.createElement("input");
							hidden.type = "hidden";
							hidden.name = "payer";
							hidden.value = username;
							formEl.appendChild(hidden);

							loadingMsgDialog = Alfresco.util.PopupManager.displayMessage({
								displayTime : 0,
								modal : true,
								text : "<span class='wait'>" + $this.msg("label.loading") + "</span>",
								noEscape : true
							});
						}
					},
					onSuccess : {
						fn : function(response) {
							var jsonResponse = response.json;

							if (loadingMsgDialog) {
								loadingMsgDialog.destroy();
							}

							Alfresco.util.PopupManager.displayMessage({

								text : $this.msg("actions.projecty.declare-as-claim.success")
							});

							YAHOO.Bubbling.fire("metadataRefresh");
						}
					},
					onFailure : {
						fn : function(response) {
							if (loadingMsgDialog) {
								loadingMsgDialog.destroy();
							}

							Alfresco.util.PopupManager.displayPrompt({
								title : $this.msg("message.failure"),
								text : $this.msg("actions.projecty.declare-as-claim.failure"),
								buttons : {
									text : $this.msg("button.ok"),
									handler : function() {
										this.destroy();
									}
								}
							});	
						}
					}
				});
				
				dialog.show();
			}
		}
	};
})();