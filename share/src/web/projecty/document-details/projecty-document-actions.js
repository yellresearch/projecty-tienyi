(function() {
 
    var extensions = {
 
        onActionDeclareAsClaim: ProjectY.actions.onActionDeclareAsClaim
     
    };
 
    /*
     * Now, augment the extensions into prototype
     */
    YAHOO.lang.augmentObject(Alfresco.DocumentActions.prototype, extensions, true);
 
})();