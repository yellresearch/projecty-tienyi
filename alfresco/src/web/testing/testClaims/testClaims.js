function testClaims() {

	var MAKE_DOCUMENT_CLAIMABLE_URL = "/alfresco/service/com/yellresearch/claim/declare?alf_ticket=";
	var RESPONSE_SUCCESS = "Expected Success value is true, returned result";

	var NODEREF = "workspace://SpacesStore/de3f4ae0-d2f1-43ab-ac40-865112494dc2";
	
	var REQUESTOR = "employee";
	var AMOUNT = 20;
	var OCCURED = "2015-02-11T00:00:00Z";
	var DUEBY = "2015-03-11T00:00:00Z";

	var userTicket = getLoginTicket();

	test("Make document claimable", function() {
		var data = $.ajax({
			async : false,
			url : MAKE_DOCUMENT_CLAIMABLE_URL + userTicket,
			type : "POST",
			data : JSON.stringify({
				nodeRef : NODEREF,
				requestor : REQUESTOR,
				amount : AMOUNT,
				occured : OCCURED,
				dueBy : DUEBY
			}),
			dataType : "json",
			contentType : 'application/json; charset=utf-8'
		})
		var response = eval("(" + data.responseText + ")");
		ok(response, "Actual Result: " + data.responseText);
		if (response) {
			equals(response.success, true, RESPONSE_SUCCESS);
		}
	});

	var REIMBURSE_CLAIM_URL = "/alfresco/service/com/yellresearch/claim/reimburse?alf_ticket=";

	var PAYER = "admin";
	var PAYMENTDATE = "2015-02-12T00:00:00Z";

	test("Reimburse claim", function() {
		var data = $.ajax({
			async : false,
			url : REIMBURSE_CLAIM_URL + userTicket,
			type : "POST",
			data : JSON.stringify({
				nodeRef : NODEREF,
				payer : PAYER,
				paymentDate : PAYMENTDATE
			}),
			dataType : "json",
			contentType : 'application/json; charset=utf-8'
		})
		var response = eval("(" + data.responseText + ")");
		ok(response, "Actual Result: " + data.responseText);
		if (response) {
			equals(response.success, true, RESPONSE_SUCCESS);
		}
	});
}