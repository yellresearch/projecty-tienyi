var nodeRef = json.has("nodeRef") ? json.get("nodeRef") : "";

if (!nodeRef){
	throw "nodeRef is undefined";
}

// search for the node
var node= search.findNode(nodeRef);

if (!node){
	throw "Invalid nodeRef. Couldn't find node with: " + nodeRef;
}

// must be a document
if (!node.isSubType("cm:content")){
	throw "Invalid node type: " + node.typeShort + ". Subtype of cm: content is required";
}

var requestor = json.has("requestor") ? json.get("requestor") : "";
var amount = json.has("amount") ? json.get("amount") : 0;
var occured = json.has("occured") ? utils.fromISO8601(json.get("occured")) : null;
var dueBy = json.has("dueBy") ? utils.fromISO8601(json.get("dueBy")) : null;
// we'll use the default value for property "isPaid": false

if (!requestor){
	throw "requestor is undefined";
}

if (!amount || isNaN(amount)){
	throw "amount is either undefined or is not a number";
}

if (!occured){
	throw "occured is either undefined or is not a valid date";
}

if (!dueBy){
	throw "dueBy is either undefined or is not a valid date";
}

if (node.hasAspect("projecty:claimable")){
	throw "Document is already a claim: " + nodeRef;
}

node.addAspect("projecty:claimable", {
	"projecty:requestor": requestor,
	"projecty:amount": amount,
	"projecty:occured": occured,
	"projecty:dueBy": dueBy
});

model.output = jsonUtils.toJSONString({
	success:true,
	responseCode: 200,
	responseMessage: "Document is now a claim",
	payLoad: {}
});