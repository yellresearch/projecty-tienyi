var nodeRef = json.has("nodeRef") ? json.get("nodeRef") : "";
 
if (!nodeRef) {
    throw "nodeRef is undefined";
}
 
// search for the node
var node = search.findNode(nodeRef);
 
if (!node) {
    throw "Invalid nodeRef. Couldn't find node with: " + nodeRef;
}
 
// must be a document
if (!node.isSubType("cm:content")) {
    throw "Invalid node type: " + node.typeShort + ". Sub-type of cm:content is required";
}

var paymentDate = json.has("paymentDate") ? json.get("paymentDate") : null;
var payer = json.has("payer") ? json.get("payer") : "";

if (!paymentDate){
	throw "paymentDate is not a valid date";
}

if (!payer){
	throw "payer is undefined";
}

if (!node.hasAspect("projecty:claimable")){
	throw "Document is not a claim: " + nodeRef;
}

node.addAspect("projecty:claimable", {
		"projecty:isPaid": true,
		"projecty:paymentDate": paymentDate,
		"projecty:payer": payer
});

model.output = jsonUtils.toJSONString({
	success: true,
	responseCode: 200,
	responseMessage: "Reimburse claim successful",
	payload: {}
});